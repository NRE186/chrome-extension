if (!localStorage.getItem('password')) {
    localStorage.setItem('password', 123)
}
if (!localStorage.getItem('sites')) {
    localStorage.setItem('sites', '')
}

const createSiteList = () => {
    let list = document.getElementById('sites-list')
    list.innerHTML = ""
    let items = localStorage.getItem('sites').split(';')
    items.forEach(item => {
        if(item != ""){
            var button = document.createElement('button')
            button.setAttribute('type','button')
            button.setAttribute('class','list-group-item list-group-item-action')
            button.innerHTML = item
            list.appendChild(button)
        }
    });
}

if(localStorage.getItem('sites')){
    createSiteList()
}

document.getElementById("addSite").onclick = () => {
    let sites = localStorage.getItem('sites')
    let site = document.getElementById('site').value
    if(site != ""){
        let p = prompt("Подтвердите действие паролем")
        if (p == localStorage.getItem('password')) {
            if (site != "") {
                sites += `;${site}`
                localStorage.setItem('sites', sites)
                document.getElementById('site').value = ""
                createSiteList()
            }
        }
    }
};

document.getElementById("changePass").onclick = () => {
    let oldPassword = localStorage.getItem('password')
    let newPassword = document.getElementById('password').value
    if(newPassword != ""){
        let p = prompt("Подтвердите действие паролем")
        if (p == oldPassword) {
            localStorage.setItem('password', newPassword)
            document.getElementById('password').value = ""
        }
    }
};

document.getElementById("show-sites").onclick = () =>{
    if(!document.getElementById("sites-list").classList.contains('hidden')){
        document.getElementById("sites-list").classList.add('hidden')
        document.getElementById('show-sites').innerHTML = "Просмотр списка сайтов"
    }
    else{
        let p = prompt("Подтвердите действие паролем")
        if (p == localStorage.getItem('password')) {
            document.getElementById("sites-list").classList.remove('hidden')
            document.getElementById('show-sites').innerHTML = "Скрыть список сайтов"
        }
    }
}

const protectSite = () => {
    while (true) {
        let pass = prompt("Введите пароль для доступа к сайту")
        if (pass == localStorage.getItem('password')) break
    }
}